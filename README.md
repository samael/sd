# README
Superdevs task

## Run
Project uses [poetry](https://python-poetry.org/) see [installation notes](https://python-poetry.org/docs/#installation).

Run `poetry install` to install or re-install all dependencies.

Run `poetry update` to update the locked dependencies to the most recent
version, honoring the constrains put inside `pyproject.toml`.

Apply migrations `poetry run python sd/manage.py migrate`.

To run webserver use command `poetry run python sd/manage.py runserver`.


