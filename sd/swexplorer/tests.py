import json

from django.test import TestCase
import responses
from swexplorer.client import SWClient


class PagesTestCase(TestCase):
    def test_index_redirect(self):
        response = self.client.get("/")
        assert response.status_code == 302
        assert response.headers["Location"] == "/collection/"

    def test_collections(self):
        response = self.client.get("/collection/")
        assert response.status_code == 200

    # TODO: More tests for other views


class SWClientTestCase(TestCase):
    def setUp(self):
        self.base_url = "http://test/"
        self.api_client = SWClient(base_url=self.base_url)

    @responses.activate
    def test_client_get_people(self):
        data = {"count": 0, "next": None, "previous": None, "results": []}
        responses.add(
            responses.GET,
            "http://test/people/?page=1",
            body=json.dumps(data),
            status=200,
        )

        data = self.api_client.get_people()

        assert data == []

    @responses.activate
    def test_client_get_people_data(self):
        data = {"count": 0, "next": None, "previous": None, "results": [{"foo": "bar"}]}
        responses.add(
            responses.GET,
            "http://test/people/?page=1",
            body=json.dumps(data),
            status=200,
        )

        data = self.api_client.get_people()

        assert data == [{"foo": "bar"}]

    @responses.activate
    def test_client_get_people_data_pages(self):
        data = [
            {
                "count": 1,
                "next": "http://test/people/?page=2",
                "previous": None,
                "results": [{"foo": "bar"}],
            },
            {"count": 1, "next": None, "previous": None, "results": [{"baz": "bar"}]},
        ]
        responses.add(
            responses.GET,
            "http://test/people/?page=1",
            body=json.dumps(data[0]),
            status=200,
        )
        responses.add(
            responses.GET,
            "http://test/people/?page=2",
            body=json.dumps(data[1]),
            status=200,
        )

        data = self.api_client.get_people()

        assert data == [{"foo": "bar"}, {"baz": "bar"}]

    # TODO: More tests for client.get_homeworlds and etl
