from django.urls import reverse_lazy
from django.views.generic import ListView, RedirectView, DetailView

from swexplorer.models import Collection


class CollectionListView(ListView):
    template_name = "list.html"
    queryset = Collection.objects.all()


class CollectionDetailView(DetailView):
    template_name = "detail.html"
    queryset = Collection.objects.all()
    rows = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        etl = kwargs["object"].etl
        try:
            context["page"] = page = int(self.request.GET.get("page", 0))
        except ValueError:
            context["page"] = page = 0

        context["rows"] = etl.rowslice(page * self.rows, (page + 1) * self.rows)
        # TODO: Check if there is a next page and if not hide 'Next' button.
        return context


class CollectionCountView(DetailView):
    template_name = "count.html"
    queryset = Collection.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        etl = kwargs["object"].etl
        fields = self.request.GET.getlist("fields")
        # TODO: Check if `fields` are not empty.
        context["counter"] = etl.valuecounts(*fields)
        return context


class FetchCollectionView(RedirectView):
    url = reverse_lazy("swexplorer:list")

    def get(self, request, *args, **kwargs):
        Collection.get_collection()
        return super().get(request, *args, **kwargs)
