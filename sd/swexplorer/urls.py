from django.urls import path
from swexplorer import views

app_name = "swexplorer"
urlpatterns = [
    path("", views.CollectionListView.as_view(), name="list"),
    path("fetch/", views.FetchCollectionView.as_view(), name="fetch"),
    path("<int:pk>/", views.CollectionDetailView.as_view(), name="detail"),
    path("<int:pk>/count/", views.CollectionCountView.as_view(), name="count"),
]
