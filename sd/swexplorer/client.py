from urllib.parse import urljoin

import requests
from django.conf import settings


class SWClient:
    def __init__(self, base_url=settings.SWAPI_URL):
        self._base_url = base_url

    def _get_paginated(self, url, field="results", params=None, **kwargs):
        data = []
        if params is None:
            params = {}
        params.setdefault("page", 1)

        while True:
            response = requests.get(
                urljoin(self._base_url, url), params=params, **kwargs
            )
            payload = response.json()
            data.extend(payload[field])
            if not payload["next"]:
                break
            params["page"] += 1
        return data

    def get_people(self):
        return self._get_paginated("people/")

    def get_homeworlds(self):
        return self._get_paginated("planets/")
