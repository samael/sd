import uuid
from tempfile import NamedTemporaryFile

import petl as etl
from django.core.files import File
from django.db import models
from django.urls import reverse
from petl import datetimeparser

from swexplorer.client import SWClient

isodatetime = datetimeparser("%Y-%m-%dT%H:%M:%S.%f%z")


class Collection(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    file = models.FileField(upload_to="swexplorer/collection/file")

    class Meta:
        ordering = ["-created"]

    def get_absolute_url(self):
        return reverse("swexplorer:detail", kwargs={"pk": self.pk})

    @property
    def etl(self):
        return etl.fromcsv(self.file.path)

    @classmethod
    def get_collection(cls):
        people, homeworlds = cls.fetch_starwars_data()

        people_table = etl.fromdicts(people)
        homeworlds_table = etl.fromdicts(homeworlds)
        homeworlds_table = homeworlds_table.cutout(*range(1, 13))
        homeworlds_table = homeworlds_table.rename("name", "homeworld_name")
        people_table = people_table.join(homeworlds_table, lkey="homeworld", rkey="url")
        people_table = people_table.addfield(
            "date", lambda v: isodatetime(v["edited"]).strftime("%Y-%m-%d")
        )
        people_table = people_table.cutout(
            "films",
            "species",
            "vehicles",
            "starships",
            "edited",
            "created",
            "url",
            "homeworld",
        )
        people_table = people_table.rename("homeworld_name", "homeworld")

        collection = cls()
        with NamedTemporaryFile() as tmpfile:
            etl.tocsv(people_table, tmpfile.name)
            collection.file.save(f"{uuid.uuid4()}.csv", content=File(tmpfile))

        collection.save()
        return collection

    @classmethod
    def fetch_starwars_data(self):
        client = SWClient()
        people = client.get_people()
        homeworlds = client.get_homeworlds()
        return people, homeworlds
